# Purpose

This repo was made for the purpose of a no more than 10-minute talk ("lightning talk") for the 2/5 CCSF Coders Club meeting. I demo'd the basic use of Yeoman, Grunt and Bower, and used PageKite to allow others to see the website.

# 2015coders

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.0.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.