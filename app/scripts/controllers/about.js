'use strict';

/**
 * @ngdoc function
 * @name 2015codersApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the 2015codersApp
 */
angular.module('2015codersApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
