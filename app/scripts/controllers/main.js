'use strict';

/**
 * @ngdoc function
 * @name 2015codersApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the 2015codersApp
 */
angular.module('2015codersApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
